# ABC Notation

ABC Notation is a preset for the Moodle activity database.

## Getting started

Download the source code and zip the files WITHOUT parent folder. Create a "Database" activity in Moodle and then upload the ZIP file in the "Presets" tab under "Import".

## Language Support

The preset is available in German. 

## Description

Students or teachers can create or import music in ABC notation. 
The music notes are playable via midi sound. 

## License

https://www.abcjs.net/#can-i

## Screenshots

<img width="400" alt="single view" src="/screenshots/einzelansicht.png">

<img width="400" alt="list view" src="/screenshots/listenansicht.png">

